///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   10_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

using namespace std;

namespace animalfarm {

   enum Gender { MALE, FEMALE, UNKNOWN };
   enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN};

   class Animal {
   public:
	   Animal();
      ~Animal();

      enum Gender gender;
	   string      species;

	   virtual const string speak() = 0;
	   virtual void printInfo();
	   string colorName  (enum Color color);
	   string genderName (enum Gender gender);
   };

}
