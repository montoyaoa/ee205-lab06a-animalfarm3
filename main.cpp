///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   23_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "animalfactory.hpp"
#include "utility.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	cout << "Welcome to Animal Farm 3" << endl;
   
   //create the animal array and fill it with null pointers
   std::array<Animal*, 30> animalArray;
   animalArray.fill(NULL);

   //fill the animal array with 25 random animals, overwriting the null pointers
   for (int i = 0; i < 25; i++)
      animalArray[i] = animalfarm::AnimalFactory::getRandomAnimal();
  
   cout << endl;
   
   //count through the array, incrementing if the animal pointer in the array is not a null pointer
   int count = 0;
   while(count <= animalArray.max_size() && animalArray[count] != NULL) 
      count++;

   cout << "Array of Animals:" << endl;
   cout << "Is it empty: " << boolalpha << animalArray.empty() << endl;
   cout << "Number of elements: " << count << endl;
   cout << "Max size: " << animalArray.max_size() << endl;
   
   cout << endl;

   //iterate through the array, calling the speak() function if the iterator is not a null pointer 
   for (auto i = animalArray.begin(); i != animalArray.end(); ++i) 
      if(*i != 0)    
         cout << (*i)->speak() << endl;

   cout << endl;

   //create the animal list and add 25 random animals to it
   std::list<Animal*> animalList;
   for (int i = 0 ; i < 25 ; i++)
      animalList.push_front(AnimalFactory::getRandomAnimal());

   cout << endl;

   cout << "List of Animals:" << endl;
   cout << "Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << "Number of elements: " << animalList.size() << endl;
   cout << "Max size: " << animalList.max_size() << endl;

   cout << endl;

   //iterate through the list, calling the speak() function
   for (auto i = animalList.begin() ; i != animalList.end() ; ++i)
      cout << (*i)->speak() << endl;

   cout << endl;

   //iterate through the animal array to clear it
   //after clearing the array, it will still exist and have max_size values, but they will not point to anything
   int j = 0; 
   for (auto i = animalArray.begin(); i != animalArray.end(); ++i) {
      //delete the currently iterated object
      delete *i;
      //clear the POINTER to the object in the array by setting it to a null pointer
      animalArray[j++] = NULL;
   }

   //another way to iterate through animalList
   //deletes the currently iterated animal object
   for( Animal* animal : animalList ) {
      delete animal;
   }
   //clears the pointers to the previously deleted animal objects in animalList
   animalList.clear();


	return 0;
}
